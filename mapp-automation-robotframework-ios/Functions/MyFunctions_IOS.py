# -*- coding: utf-8 -*-
# from appium.webdriver.common.touch_action import TouchAction
#  
# from AppiumLibrary.locators import ElementFinder
# from .keywordgroup import KeywordGroup
#  
#  
#  
# class MyFunctions(KeywordGroup):
#      
#     def __init__(self):
#         self._element_finder = ElementFinder()
#          
#     def Click_And_Clear_Text_From_Field(self, locator):
#         driver = self._current_application()
#         el = self._element_finder(locator, True, True)
#         driver.click_element(element=el).perform()
#         driver.clean(element=el).perform()


#from appium import webdriver
from AppiumLibrary.locators import ElementFinder
from AppiumLibrary import _ElementKeywords, AppiumLibrary
from robot.libraries.BuiltIn import BuiltIn
import ast
from unicodedata import normalize
from selenium.webdriver.remote.webelement import WebElement
import csv

class MyFunctions_IOS(AppiumLibrary,_ElementKeywords):
   """This is an example library with some documentation."""
   ROBOT_LIBRARY_VERSION = "1.0.2";
   ROBOT_LIBRARY_SCOPE = "GLOBAL";
   
   def Get_Count_Of_Elements(self, locator):
       """This is an example library with some documentation."""
       elementCount = self.get_matching_xpath_count(self, '//*')
       print("Count of elements: ", elementCount)
       self._element_find(locator, True, True).click()
    
   def Read_csv_file(self, filename):
       '''This creates a keyword named "Read CSV File"
       This keyword takes one argument, which is a path to a .csv file. It
       returns a list of rows, with each row being a list of the data in
       each column.'''
       data = []
       with open(filename, 'rt') as csvfile:
           reader = csv.reader(csvfile)
           i=0
           for row in reader:
               # Skip headers in file
               if i>0:
                   data.append(row)
               i=i+1      
       return data

   def get_Data_Length(self, data):
       return len(data)
   
   def KeyboardType(self, textToType):
       AppiumLibrary._current_application().execute_script("var vKeyboard = target.frontMostApp().keyboard(); vKeyboard.typeString(\"" + textToType + "\");")
   
   # Calculate position X on screen for input resolution in comparison to default resolution (1080x1920)    
   def Get_X(self, locationKeyX, resolutionX):
       defaultResX=1080
       
       multiplier = float(defaultResX) / float(resolutionX)
       
       return float(locationKeyX) / float(multiplier)
   
   # Calculate position Y on screen for input resolution in comparison to default resolution (1080x1920)        
   def Get_Y(self, locationKeyY, resolutionY):
       defaultResY=1920
       
       multiplier = float(defaultResY) / float(resolutionY)
       
       return float(locationKeyY) / float(multiplier)
    
   def Swipe_IOS_Next_Button(self):
    
       mid_x = get_x_location_of_next_swipe_button_at_ios()
       mid_y = get_y_location_of_next_swipe_button_at_ios()
       end_x = mid_x + 1000
       end_y = mid_y + 1000
       act = TouchAction(self.driver)
       act.press(mid_x, mid_y).move_to(end_x, end_y).release()
       act.perform()

    
   def Remove_Text_From_Field(self, locator):
       driver = self._current_application()
       el = driver.find_element_by_xpath(locator)
       el.clear()
       
           
   def Click_Element_At_Center(self, locator):
       driver = self._current_application()
#        table_at_af = []
#        table_at_af = driver.find_elements_by_xpath(locator)
#        print(table_at_af)
       el = driver.find_element_by_xpath(locator)
       x_cor = el.get_attribute('x')
       y_cor = el.get_attribute('y')
       width_e =  el.get_attribute('width')
       height_e = el.get_attribute('height')
       x_t_clk = x_cor + (width_e/2)
       y_t_clk = y_cor + (height_e/2)
       driver.press(x_t_clk, y_t_clk)  
       
   def Refresh_The_Page(self):
       driver = self._current_application()
       driver.getPageSource()         
       
       
       
        