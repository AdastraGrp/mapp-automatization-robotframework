*** Settings ***
# Library    AppiumLibrary  10
Library    MyFunctions_IOS

*** Variables ***
${SETTINGS_IN_TAPBAR}    Settings
#//XCUIElementTypeButton[@name="Settings"]    

*** Keywords ***
Go to Settings
    Wait Until Element Is Visible    ${SETTINGS_IN_TAPBAR}    120
    Tap    ${SETTINGS_IN_TAPBAR} 