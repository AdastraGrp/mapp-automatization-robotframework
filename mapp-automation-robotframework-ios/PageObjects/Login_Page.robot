*** Settings ***
# Library    AppiumLibrary  10
Library    MyFunctions_IOS
Library    BuiltIn    

*** Variables ***
${PHONE_NUMBER_WHOLE_FIELD}    //XCUIElementTypeApplication[@name="捷信惠购商户版"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTextField[1]
${PHONE_NUMBER}    15896322222
# //XCUIElementTypeButton[@name="Login"] - when "Login" will not work
${LOGIN_BUTTON_}    Login 
${PIN_NUMBER_ONE}    //XCUIElementTypeApplication[@name="捷信惠购商户版"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther
${PIN_NUMBER_TWO}    //XCUIElementTypeApplication[@name="捷信惠购商户版"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]

*** Keywords ***
Login
    Tap    ${PHONE_NUMBER_WHOLE_FIELD}
    Input Text    ${PHONE_NUMBER_WHOLE_FIELD}    ${PHONE_NUMBER}
    Tap    ${LOGIN_BUTTON_}
    
PIN Input
    #Wait Until Page Contains Element    ${PIN_NUMBER_ONE}    120
    Wait Until Element Is Visible    ${PIN_NUMBER_ONE}    120
    #Sleep    25000ms
    Tap    ${PIN_NUMBER_ONE}
    Tap    ${PIN_NUMBER_TWO}
    Tap    ${PIN_NUMBER_ONE}
    Tap    ${PIN_NUMBER_TWO}
    Tap    ${PIN_NUMBER_ONE}
    Tap    ${PIN_NUMBER_TWO}
    

