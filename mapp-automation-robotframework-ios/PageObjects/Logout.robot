*** Settings ***
# Library    AppiumLibrary  10
Library    MyFunctions_IOS

*** Variables ***
${LOGOUT_BUTTON}    //XCUIElementTypeApplication[@name="捷信惠购商户版"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[8]    
${YES_IN_POPUP_LOGOUT}    Yes
#//XCUIElementTypeButton[@name="Yes"]            

*** Keywords ***
Logout
    Wait Until Element Is Visible    ${LOGOUT_BUTTON}    15
    Tap    ${LOGOUT_BUTTON}
    Wait Until Element Is Visible    ${YES_IN_POPUP_LOGOUT}    5
    Tap    ${YES_IN_POPUP_LOGOUT} 