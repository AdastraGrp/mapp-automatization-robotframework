[Dodumentation]
 - Application is for each test case installed again
 - After test is application closed
 - For keeping independency of test cases is application installed again for each test case. For developing is possible to uncomment row for Reset application afetr test case 


*** Settings ***
Library        MyFunctions_IOS
Resource        ../Resources/properties.robot

# Rewrite to Suite Setup, when you develop and you want to have quicker restart
Suite Setup  Open Application  
...  ${APPIUM_IP_ADDRESS}  
...  udid=${deviceId}  
...  platformName=${PLATFORM_NAME}
...  deviceName=${deviceName}
...  app=${app_path} 
...  autoAcceptAlerts=True
...  automationName=XCUITest 

# Replace by Test Teardown    Reset Application for developing - quicker run of test cases       
Suite Teardown    End Test

# Uncomment for developing - app is not isntalled again, is only reseted
# Test Teardown    Reset Application

*** Keywords ***
    
End test
    [Documentation]    After end of test get screen shot  - use in tear down script
    Run Keyword If Any Critical Tests Failed    Capture Page Screenshot    FailedTest_iOS.png
    Close Application
    
Reopen App
    Sleep    1000ms 
    Background App    seconds=3    #    Appium fell down here, this kind of refresh keeps appium alive
    Sleep    1000ms 